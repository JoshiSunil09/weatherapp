//
//  String+Extension.swift
//  WeatherApp
//
//  Created by mac on 20/11/23.
//

import Foundation
//MARK: - to convert timestamp to date
extension String {
    func convertTimestampToString() -> String {
        let date = Date(timeIntervalSince1970: Double(self) ?? 0.0)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
        dateFormatter.timeZone = .current
        let localDate = dateFormatter.string(from: date)
        return localDate
    }
    
    // check white space and blank lines
    var trimWhiteSpace: String {
        let trimmedString = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return trimmedString
    }
}
