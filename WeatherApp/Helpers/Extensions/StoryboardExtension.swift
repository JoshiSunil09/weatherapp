//
//  StoryboardExtension.swift
//  WeatherApp
//
//  Created by mac on 20/11/23.
//

import Foundation
import Foundation
import UIKit

enum AppStoryboard: String {
case weather = "WeatherDetails"
}
extension AppStoryboard {
var instance: UIStoryboard {
    return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
}
    func viewController<T: UIViewController>(_ viewControllerClass: T.Type, function: String = #function, line: Int = #line, file: String = #file) -> T {
        
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        guard let scene = instance.instantiateViewController(withIdentifier: storyboardID) as? T else {
            fatalError("ViewController with identifier \(storyboardID), not found in \(self.rawValue) Storyboard.\nFile : \(file) \nLine Number : \(line) \nFunction : \(function)")
        }
        return scene
    }

}
