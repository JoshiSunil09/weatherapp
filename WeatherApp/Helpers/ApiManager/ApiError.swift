//
//  ApiError.swift
//  WeatherApp
//
//  Created by mac on 20/11/23.
//

import Foundation
struct APIError: Decodable, Error {
    
    let status: String
    let message: String
    let errorCode: String?
    let errors: [APIError]?
}
