//
//  StringConstants.swift
//  WeatherApp
//
//  Created by mac on 21/11/23.
//

import Foundation

struct StringConstants {
    static let WeatherLandingTitle = "Weather Details"
    static let enableLocationServiceAlert = "Location service is not enabled"
    static let enableLocationDesc = "Please enable location to get weather data"
    static let enableLocationService = "Please enable location service"
    static let searchCityTitle = "Check City Weather"
    
}
