//
//  WeatherApiManager.swift
//  WeatherApp
//
//  Created by mac on 20/11/23.
//

import Foundation
import UIKit
import Combine
import Alamofire

class NetworkHelper {
    
    static let session: Session = {
        let monitor = ClosureEventMonitor()
        monitor.requestDidCompleteTaskWithError = { request, _, _ in
            debugPrint(request)
        }
        return Session(eventMonitors: [monitor])
    }()
    
    static func request<Element: Decodable>(url: String, method: HTTPMethod, param: [String: Any] = [:], headers: HTTPHeaders? = nil, parameterEncoding: ParameterEncoding? = nil, _ responseType: Element.Type = Element.self, controller: UIViewController? = nil) -> AnyPublisher<DataResponse<Element, Error>, Never> {
        let encoding: ParameterEncoding
        if let parameterEncoding = parameterEncoding {
            encoding = parameterEncoding
        } else {
            if method == .patch || method == .put || method == .post {
                encoding = JSONEncoding.default
            } else { encoding = URLEncoding.default }
        }
        return session.request(url, method: method, parameters: param, encoding: encoding, headers: headers)
            .validate()
            .publishDecodable(type: Element.self)
            .map { response in
                return response.mapError { error -> Error in
                    func logError() {
                        var reportParam = param
                        reportParam["URL"] = url
                        if let data = response.data, let originalResponse = String(data: data, encoding: .utf8) {
                            reportParam["original-response"] = originalResponse
                        }
                        reportParam[NSLocalizedDescriptionKey] = error.localizedDescription
                    }
                    if response.response?.statusCode == 204 {
                        return NSError(domain: "NoContent", code: 204, userInfo: [NSLocalizedDescriptionKey: "Object is empty"])
                    }
                    if error.isResponseSerializationError || error.isResponseValidationError {
                        let apiError = response.data.flatMap { try? JSONDecoder().decode(APIError.self, from: $0) }
                        if let apiError = apiError {
                            logError()
                            showErrorDialog(controller: controller, errorMessage: apiError.message)
                            return NSError(domain: "APIError", code: Int(apiError.status) ?? (error as NSError).code, userInfo: [NSLocalizedDescriptionKey: apiError.message])
                        }
                    }
                    if error.responseCode == 404 {
                        let errorMessage = "The page you are looking for doesn't exist."
                        showErrorDialog(controller: controller, errorMessage: errorMessage)
                        return NSError(domain: "APIError", code: 404, userInfo: [NSLocalizedDescriptionKey: errorMessage])
                    }
                    if error.isExplicitlyCancelledError {
                        return error
                    }
                    switch (error as NSError).code {
                    case -999:
                        break
                    case -1_009:
                        showErrorDialog(controller: controller, errorMessage: error.localizedDescription)
                    case 4, NSFormattingError, 4_865, 4_864:
                        logError()
                        let errorMessage = "Something went wrong. Check your connection and try again."
                        showErrorDialog(controller: controller, errorMessage: errorMessage)
                        return NSError(domain: (error as NSError).domain, code: (error as NSError).code, userInfo: [NSLocalizedDescriptionKey: errorMessage])
                    default:
                        logError()
                        showErrorDialog(controller: controller, errorMessage: error.localizedDescription)
                    }
                    return error
                }
            }
            .eraseToAnyPublisher()
    }
    
    private static func showErrorDialog(controller: UIViewController?, errorMessage: String) {
        controller?.showAlert(title: "Error", msg: errorMessage)
    }
}
