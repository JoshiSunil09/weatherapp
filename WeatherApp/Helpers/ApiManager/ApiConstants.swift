//
//  ApiConstants.swift
//  WeatherApp
//
//  Created by mac on 20/11/23.
//

import Foundation

class ApiPath {
    
    static let shared = ApiPath()
    var apiKey = "c30eb70a79e2ead7b78468e15738165e"
    var baseURL = "https://api.openweathermap.org/data/2.5/weather"
    var metricValue = "metric"
}
