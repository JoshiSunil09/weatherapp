//
//  WeatherModel.swift
//  WeatherApp
//
//  Created by mac on 20/11/23.
//

import Foundation

struct WeatherModel: Codable {
    let weather: [Weather]
    let main: Main
    let name: String
    let dt : Int?
    var cityTemprature: String {
        return String(format: "%.2f", main.temp)
    }
    var description: String {
        return weather.first?.description ?? ""
    }
    var icon: String {
        return weather.first?.icon ?? "help"
    }
}

struct Main: Codable {
    let temp: Double
}

struct Weather: Codable {
    let main, description, icon: String
}
