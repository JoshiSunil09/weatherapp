//
//  WeatherDetailViewModel.swift
//  WeatherApp
//
//  Created by mac on 20/11/23.
//

import Foundation
import UIKit
import CoreLocation
import Combine

enum weatherResponseOption {
    case error(Error)
    case success(WeatherModel)
}

protocol WeatherViewModelProtocols {
    var getWeatherDetails: ((weatherResponseOption) -> Void) { get set }
    var locationServiceNotEnabled : ((Bool) -> Void) {get set}
}

class WeatherDetailViewModel: NSObject {
    
    var token: AnyCancellable?
    private let locationManager = CLLocationManager()
    var getWeatherDetails: ((weatherResponseOption) -> Void) = {_ in }
    var locationServiceNotEnabled : ((Bool) -> Void) = {_ in }
    
    
    override init() {
        super.init()
    }
    
    //MARK: - call api for get weather details of current location
    func callApiForGetWeatherDetails(location: CLLocation) {
        let param = ["lat": String(location.coordinate.latitude),
                     "lon": String(location.coordinate.longitude),
                     "appid": ApiPath.shared.apiKey,
                     "units": ApiPath.shared.metricValue
        ]
        token = NetworkHelper.request(url: ApiPath.shared.baseURL, method: .get, param: param, WeatherModel.self).sink { [self] response in
            switch response.result {
            case .success(let response):
                getWeatherDetails(.success(response))
            case .failure(let error):
                print(error)
                getWeatherDetails(.error(error))
            }
        }
    }
}

//MARK: - Location manager delegate methods
extension WeatherDetailViewModel: CLLocationManagerDelegate {
    func startLocationTracking() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 100
        DispatchQueue.global().async { [weak self] in
            guard let self else { return }
            if CLLocationManager.locationServicesEnabled() {
                switch locationManager.authorizationStatus {
                case .notDetermined:
                    locationManager.requestWhenInUseAuthorization()
                case  .restricted, .denied:
                    break
                case .authorizedAlways, .authorizedWhenInUse:
                    locationManager.startUpdatingLocation()
                @unknown default:
                    break
                }
            } else {
                locationServiceNotEnabled(true)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .authorizedWhenInUse,
                .authorizedAlways:
            locationManager.startUpdatingLocation()
        case .restricted, .denied:
            locationServiceNotEnabled(true)
            break
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        self.callApiForGetWeatherDetails(location: location)
    }
    
}
