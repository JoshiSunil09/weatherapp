//
//  SearchCityViewModel.swift
//  WeatherApp
//
//  Created by mac on 20/11/23.
//

import Foundation
import Combine
import UIKit

class SearchCityViewModel: NSObject {
    
    var token: AnyCancellable?
    var getWeatherDetails: ((weatherResponseOption) -> Void) = {_ in }
    
    override init() {
        super.init()
    }
    
    //MARK: - Api call for get city weather detail
    func callApiForGetCityWeather(city: String) {
        let param = ["q": city,
                     "appid": ApiPath.shared.apiKey,
                     "units": ApiPath.shared.metricValue
        ]
        token = NetworkHelper.request(url: ApiPath.shared.baseURL, method: .get, param: param, WeatherModel.self).sink { [self] response in
            switch response.result {
            case .success(let response):
                getWeatherDetails(.success(response))
            case .failure(let error):
                print(error)
                getWeatherDetails(.error(error))
            }
        }
    }
}
