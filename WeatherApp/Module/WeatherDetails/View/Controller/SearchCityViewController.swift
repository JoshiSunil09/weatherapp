//
//  SearchCityViewController.swift
//  WeatherApp
//
//  Created by mac on 20/11/23.
//

import UIKit

class SearchCityViewController: UIViewController {
    
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var tempratureLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var searchTextField: UITextField!
    
    var currentCityName = ""
    
    lazy var viewModel: SearchCityViewModel = {
        let viewModel = SearchCityViewModel()
        return viewModel
    }()

    //MARK: - UIViewControllerLifeCycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = StringConstants.searchCityTitle
        self.setUpViewModelBinding()
        viewModel.callApiForGetCityWeather(city: self.currentCityName)
    }
    
    //MARK: - Viewmodel binding
    private func setUpViewModelBinding() {
        viewModel.getWeatherDetails = { [weak self] option in
            guard let self else { return }
            switch option {
            case .success(let weather):
                self.setupCityData(weatherObj: weather)
            case .error(let error):
                // can show error or required alert
                print("error", error)
            }
        }
    }
    
    //MARK: - Setup screen data
   private func setupCityData(weatherObj: WeatherModel) {
        self.locationLabel.text = weatherObj.name
        self.tempratureLabel.text = "\(weatherObj.cityTemprature)\("°C")"
        self.weatherImage.image = UIImage(named: weatherObj.icon)
        self.weatherLabel.text = weatherObj.description
    }
}

//MARK: - UITextField delegate methods
extension SearchCityViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchTextField.endEditing(true)
        if let cityName = searchTextField.text?.trimWhiteSpace, !(searchTextField.text?.trimWhiteSpace.isEmpty ?? false) {
            viewModel.callApiForGetCityWeather(city: cityName)
        }
        searchTextField.text = ""
        return true
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.text?.isEmpty == false {
            return true
        } else {
            return false
        }
    }
}
