//
//  WeatherLandingViewController.swift
//  WeatherApp
//
//  Created by mac on 20/11/23.
//

import UIKit

class WeatherLandingViewController: UIViewController {
    
    @IBOutlet weak var weatherTypeDescription: UILabel!
    @IBOutlet weak var weatherTypeIcon: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var tempratureLabel: UILabel!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var checkWeather: UIButton!
    
    private var currentCityName = ""
    
    lazy var viewModel: WeatherDetailViewModel = {
        let viewModel = WeatherDetailViewModel()
        return viewModel
    }()

    //MARK: - UIViewControllerLifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialMethod()
    }
    
    //MARK: Helper methods
    private func initialMethod() {
        self.title = StringConstants.WeatherLandingTitle
        self.navigationController?.navigationBar.isHidden = false
        viewModel.startLocationTracking()
        self.setUpViewModelBinding()
    }
    
    //MARK: - View model binding
    private func setUpViewModelBinding() {
        viewModel.getWeatherDetails = { [weak self] option in
            guard let self else { return }
            switch option {
            case .success(let weather):
                self.setupWeatherData(weatherObj: weather)
            case .error(let error):
                // can show error or required alert
                print("error", error)
            }
        }
        
        viewModel.locationServiceNotEnabled = { [weak self] status in
            guard let self else { return }
            if status {
                self.showAlert(msg: StringConstants.enableLocationServiceAlert)
                self.weatherTypeDescription.text = StringConstants.enableLocationDesc
            }
        }
    }
    
    //MARK: - set up screen data
    private func setupWeatherData(weatherObj: WeatherModel) {
        self.cityNameLabel.text = weatherObj.name
        self.tempratureLabel.text = "\(weatherObj.cityTemprature)\("°C")"
        self.weatherTypeIcon.image = UIImage(named: weatherObj.icon)
        self.weatherTypeDescription.text = weatherObj.description
        self.dateLabel.text = ("\(weatherObj.dt ?? 0)").convertTimestampToString()
        self.currentCityName = weatherObj.name
    }

        
    //MARK: - UIButton Actions
    //navigate to search city controller
    @IBAction func checkWeatherAction(_ sender: UIButton) {
        if !self.currentCityName.isEmpty {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchCityViewController")as? SearchCityViewController {
                vc.currentCityName = self.currentCityName
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            self.showAlert(msg: StringConstants.enableLocationService)
        }
    }
    
}
